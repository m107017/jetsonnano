# OpenCVによるカメラ制御
# Raspberry PiカメラモジュールV2使用

import cv2

print("OpenCVによるカメラ制御プログラム開始")

# GStreamerの設定
gst = "nvarguscamerasrc ! " \
      "video/x-raw(memory:NVMM),width=320,height=240," \
      "framerate=30/1,format=NV12 ! " \
      "nvvidconv ! " \
      "video/x-raw,format=BGRx ! " \
      "videoconvert ! " \
      "appsink drop=true sync=false"

# OpenCVのビデオキャプチャでカメラを開く
video_capture = cv2.VideoCapture(gst, cv2.CAP_GSTREAMER)

while True:
    # カメラからキャプチャする
    # readメソッドは、画像が読み込めたかどうかを示すbool値と画像の配列の2つのタプルを返す
    is_grabbed, image = video_capture.read()
    if not is_grabbed:
        print("Video Capture Error.")
        break

    # ウィンドウにキャプチャした画像を表示
    cv2.imshow("Camera Test", image)

    # 何かキーを押されたら終了
    # waitKeyメソッドの引数はキー入力待ち時間[ms]、戻り値は押されたキーのコード（何も押されていない場合は-1）。
    if cv2.waitKey(10) > -1:
        print("Video Capture End.")
        video_capture.release()
        cv2.destroyAllWindows()
        break

print("プログラム終了")
