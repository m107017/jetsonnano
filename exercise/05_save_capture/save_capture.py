# OpenCVによるカメラ画像保存
# Raspberry PiカメラモジュールV2使用

import bottle
import time
from image_capture import ImageCaptureCv2Gst, ImageWriterCv2Mp4

print("OpenCVによるカメラ画像保存プログラム開始")

# WebサーバとしてBottleを使用
web = bottle.Bottle()


@web.route("/")
def index():
    return bottle.static_file("index.html", root="./")


@web.route("/video_recv")
def video_recv():
    # GStreamerの設定
    gst = "nvarguscamerasrc ! " \
        "video/x-raw(memory:NVMM),width=320,height=240," \
        "framerate=30/1,format=NV12 ! " \
        "nvvidconv ! " \
        "video/x-raw,format=BGRx ! " \
        "videoconvert ! " \
        "appsink drop=true sync=false"

    # OpenCVのビデオキャプチャでカメラを開く
    img_capture = ImageCaptureCv2Gst()
    if img_capture.camera_open(gst) is None:
        print("プログラム終了")
        exit()
    
    # 画像出力オブジェクト（MP4形式）生成
    img_writer = ImageWriterCv2Mp4(img_capture)
    img_writer.create(320, 240, 30.0)

    bottle.response.content_type = "multipart/x-mixed-replace;boundary=frame"

    while True:
        # カメラから画像をキャプチャ
        image = img_capture.capture()
        if image is None:
            break

        # キャプチャした画像をファイル出力
        img_writer.write(image)

        # 取得した画像をJPEGに変換
        jpeg_image = img_capture.to_jpeg(image, 80)
        if jpeg_image is not None:
            yield b"--frame\r\n" + b"Content-Type: image/jpeg\r\n\r\n" + bytearray(jpeg_image) + b"\r\n\r\n"
            time.sleep(1 / 60)

    img_capture.camera_release()

    return 0


web.run(host="0.0.0.0", port=8080)

print("プログラム終了")
