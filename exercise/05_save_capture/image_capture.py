from abc import ABC, abstractmethod
import cv2


class IImageCaptureCv2(ABC):

    """OpenCV制御用クラス
    """

    def __init__(self):
        """コンストラクタ
        """

        self._video_capture = None
        self.__window_captions = set()

    @abstractmethod
    def camera_open(self, *args):
        """カメラオープン
        """
        pass

    def camera_release(self):
        """カメラ解放
        """
        self._video_capture.release()
        for caption in self.__window_captions:
            cv2.destroyWindow(caption)
        self._video_capture = None

    def capture(self):
        """画像取得

        Returns
        -------
        numpy.ndarray
            取得した画像データ（エラー時はNone）
        """
        is_grabbed, image = self._video_capture.read()
        if not is_grabbed:
            return None
        return image

    def capture_and_show(self, caption=""):
        """画像取得＆ウィンドウへの表示

        Parameters
        ----------
        caption : str, optional
            ウィンドウのキャプション名（指定がない場合は""）

        Returns
        -------
        bool
            処理が成功したか否か
        """
        image = self.capture()
        if image is None:
            return False
        cv2.imshow(caption, image)
        self.__window_captions.add(caption)
        return True

    def to_jpeg(self, src_image, compress_rate):
        """JPEG形式に変換

        Parameters
        ----------
        src_image : numpy.ndarray
            変換対象の画像データ
        compress_rate : int
            圧縮率

        Returns
        -------
        numpy.ndarray
            変換後の画像データ（ただし、エラーがある場合はNone）
        """
        if compress_rate > 100:
            return None
        is_success, dst_image = cv2.imencode(".jpg", img=src_image, params=[
            int(cv2.IMWRITE_JPEG_QUALITY), compress_rate])
        if not is_success:
            return None
        return dst_image


class ImageCaptureCv2Gst(IImageCaptureCv2):

    """OpenCV制御用クラス（GStreamer使用）

    Raspberry PiカメラモジュールV2使用時は本クラスを使用する。

    """

    def camera_open(self, *args):
        """カメラオープン

        Parameters
        ----------
        *args : (str)
            1番目 : GStreamerの設定

        Returns
        -------
        cv2.VideoCapture
            VideoCaptureオブジェクト（エラー時はNone）
        """
        self._video_capture = cv2.VideoCapture(args[0], cv2.CAP_GSTREAMER)
        if not self._video_capture.isOpened():
            return None
        return self._video_capture


class IImageWriterCv2(ABC):

    """OpenCVによる画像データ出力クラス
    """

    def __init__(self, capture: IImageCaptureCv2):
        """コンストラクタ

        Parameters
        ----------
        capture : IImageCaptureCv2
            OpenCV制御用クラスのインスタンス
        """
        self._capture = capture
        self._writer = None

    @abstractmethod
    def create(self, width, height, fps, file_name):
        """画像出力オブジェクト生成

        Parameters
        ----------
        width : int
            横方向解像度
        height : int
            縦方向解像度
        fps : double
            フレームレート
        file_name : str
            保存ファイル名
        """
        pass

    def write(self, image):
        """画像データ出力

        Parameters
        ----------
        image : numpy.ndarray
            出力対象の画像データ（1フレーム分）
        """
        if self._writer is None:
            return
        self._writer.write(image)
        pass


class ImageWriterCv2Mp4(IImageWriterCv2):

    """OpenCVによる画像データ出力クラス（MP4形式）
    """

    def create(self, width, height, fps, file_name="video"):
        """画像出力オブジェクト生成

        Parameters
        ----------
        width : int
            横方向解像度
        height : int
            縦方向解像度
        fps : double
            フレームレート
        file_name : str
            保存ファイル名
        """
        fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
        self._writer = cv2.VideoWriter(
            f"{file_name}.mp4", fourcc, fps, (width, height))
