# LED点灯制御プログラム

import Jetson.GPIO as GPIO
import time

GPIO.setwarnings(False)             # プログラム実行後にGPIO設定をクリアしていないために発生するwarningを抑制
GPIO.setmode(GPIO.BOARD)            # GPIOの番号をピン番号で指定するモードに設定
GPIO.setup(40, GPIO.OUT)            # ピン40のGPIOを出力に設定

for count in range(5):
    GPIO.output(40, GPIO.HIGH)
    time.sleep(0.5)
    GPIO.output(40, GPIO.LOW)
    time.sleep(0.5)

GPIO.cleanup()                      # GPIO設定をクリア
