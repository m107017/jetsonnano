# スイッチ入力によるLED点灯制御プログラム

import Jetson.GPIO as GPIO
import time

print("スイッチ入力によるLED点灯制御プログラム開始")

GPIO.setwarnings(False)             # プログラム実行後にGPIO設定をクリアしていないために発生するwarningを抑制
GPIO.setmode(GPIO.BOARD)            # GPIOの番号をピン番号で指定するモードに設定
GPIO.setup(40, GPIO.OUT)            # ピン40のGPIOを出力に設定
GPIO.setup(37, GPIO.IN)             # ピン37のGPIOを入力に設定

break_timer = 0.0                   # プログラム終了判定用カウンタ
while True:
    if GPIO.input(37) == GPIO.LOW:  # スイッチが押されたらLow出力（Active Low）
        GPIO.output(40, GPIO.HIGH)
        break_timer += 0.1
        if break_timer >= 3.0:
            break                   # 指定時間経過したら、プログラム終了
    else:
        GPIO.output(40, GPIO.LOW)
        break_timer = 0.0
    time.sleep(0.1)                 # チャタリング防止のため、一定時間待つ

GPIO.output(40, GPIO.LOW)
GPIO.cleanup()                      # GPIO設定をクリア

print("プログラム終了")
